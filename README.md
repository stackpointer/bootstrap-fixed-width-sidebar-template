# [Bootstrap 3 Fixed Width Sidebar Template](https://stackpointer.io/design/bootstrap-fixed-width-sidebar/555/)

A Bootstrap 3 template to create fixed width sidebars.

## Introduction

This template creates 2 css styles: `col-fixed` and `col-fluid` that will be used to replace the standard Bootstrap `col-*` grid classes. 
* `col-fixed` has a fixed width and is used for the sidebar
* `col-fluid` uses the remaining width and is used for the main content

In this template, `col-fixed` is set to 350 pixels. Note that on mobile (<768px), it will be set to 100% width. 

## Demo

For a demo, do the following:
* Clone the repo: `git clone https://gitlab.com/stackpointer/bootstrap-fixed-width-sidebar-template.git`
* Open `index.html` from browser
* Resize browser to verify

## Creator

Bootstrap 3 Fixed Width Sidebar Template was created by and is maintained by **[Mohamed Ibrahim](https://stackpointer.io/)**.

* https://twitter.com/sudo_ibrahim
* https://gitlab.com/ibrahim

This template is based on the [Bootstrap](http://getbootstrap.com/) framework.

## Copyright and License

Copyright 2008-2016 Stack Pointer. Code released under the [MIT](LICENSE) license.